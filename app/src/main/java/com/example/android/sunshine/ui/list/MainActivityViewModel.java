package com.example.android.sunshine.ui.list;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;

import com.example.android.sunshine.data.ListViewWeatherEntry;
import com.example.android.sunshine.data.SunshineRepository;

import java.util.List;


class MainActivityViewModel extends ViewModel{

    private LiveData<List<ListViewWeatherEntry>> mWeather;

    MainActivityViewModel(SunshineRepository repository) {
        mWeather = repository.getCurrentWeatherForecasts();
    }

    LiveData<List<ListViewWeatherEntry>> getCurrentWeatherForecasts() {
        return mWeather;
    }
}
